
#include <stack>
#include <boost/lexical_cast.hpp>

#include "plist-array.hpp"
#include "plist-any.hpp"
#include "plist-xmlparser.hpp"

class XmlContext
{
public:
  XmlContext(const xmlChar * elem)
    : element((const char*)elem)
  {
  }
  virtual ~XmlContext()
  {
  }
  std::string element;
};

class KeyXmlContext
  : public XmlContext
{
public:
  KeyXmlContext(const xmlChar *elem)
    : XmlContext(elem)
  {
  }
  std::string key;
};

class ValXmlContext
  : public XmlContext
{
public:
  ValXmlContext(const xmlChar *elem)
    : XmlContext(elem)
  {
  }
  PlistAny value;
};

class ArrayXmlContext
  : public ValXmlContext
{
public:
  ArrayXmlContext(const xmlChar *elem)
    : ValXmlContext(elem)
    , array(new PlistArray)
  {
    value = PlistAny(array);
  }
  PlistArray::Ptr array;
};

class DictXmlContext
  : public ValXmlContext
{
public:
  DictXmlContext(const xmlChar *elem)
    : ValXmlContext(elem)
    , dict(new PlistDict)
  {
    value = PlistAny(dict);
  }
  PlistDict::Ptr dict;
  std::string last_key;
};


class PlistXmlParser::Priv
{
public:
  PlistDict::Ptr          m_plist;
  std::stack<XmlContext*> m_stack;
};



PlistXmlParser::PlistXmlParser()
  : m_priv(new Priv)
{
}


PlistXmlParser::~PlistXmlParser()
{
  delete m_priv;
}


PlistDict::Ptr PlistXmlParser::parseFile(const std::string & filename)
{
  return parseFile(filename.c_str());
}


PlistDict::Ptr PlistXmlParser::parseFile(const char * filename)
{
  PlistDict::Ptr plist;
  
  xmlTextReaderPtr reader = xmlNewTextReaderFilename(filename);
  
  plist = _parse(reader);
  
  xmlFreeTextReader(reader);
  
  return plist;
}

bool PlistXmlParser::readText(xmlTextReaderPtr reader)
{
  KeyXmlContext * keyContext = NULL;
  ValXmlContext * context = dynamic_cast<ValXmlContext*>(m_priv->m_stack.top());
  if(context == NULL) {
    keyContext = dynamic_cast<KeyXmlContext*> (m_priv->m_stack.top());
    if(keyContext == NULL) {
      fprintf(stderr, "no context\n");
      return false;
    }
  }
  
  std::string value;
  const xmlChar * text = xmlTextReaderConstValue(reader);
  if(text) {
    value = (const char*)text;
  }
  
  try {
    if(keyContext) {
      keyContext->key = value;
    }
    else {
      if(context->element == "integer") {
	int64_t intval = boost::lexical_cast<int64_t>(value);
	context->value = PlistAny(intval);
      }
      else if(context->element == "real") {
	double realval = boost::lexical_cast<double>(value);
	context->value = PlistAny(realval);
      }
      // <string>, <date>, <data>
      else {
	context->value = PlistAny(value);
      }
    }
  }
  catch(const std::exception & e) {
    fprintf(stderr, "exception %s\n", e.what());
    return false;
  }
  return true;
}

bool PlistXmlParser::startElement(xmlTextReaderPtr reader)
{
  XmlContext *context = NULL;
  const xmlChar *name = xmlTextReaderConstName(reader);
  if(xmlStrEqual(name, BAD_CAST"plist")) {
    if(!m_priv->m_stack.empty()) {
      return false;
    }
    context = new XmlContext(name);
  }
  else if(xmlStrEqual(name, BAD_CAST"dict")) {
    context = new DictXmlContext(name);
  }
  else if(xmlStrEqual(name, BAD_CAST"array")) {
    context = new ArrayXmlContext(name);
  }
  else if(xmlStrEqual(name, BAD_CAST"true")) {
    context = new ValXmlContext(name);
    static_cast<ValXmlContext*>(context)->value = PlistAny(true);
  }
  else if(xmlStrEqual(name, BAD_CAST"false")) {
    context = new ValXmlContext(name);
    static_cast<ValXmlContext*>(context)->value = PlistAny(false);
  }
  else if(xmlStrEqual(name, BAD_CAST"integer") ||
	  xmlStrEqual(name, BAD_CAST"real") ||
	  xmlStrEqual(name, BAD_CAST"string") ||
	  xmlStrEqual(name, BAD_CAST"date") ||
	  xmlStrEqual(name, BAD_CAST"data")) {
    context = new ValXmlContext(name);
  }
  else if(xmlStrEqual(name, BAD_CAST"key")) {
    context = new KeyXmlContext(name);
  }
  else {
    context = new XmlContext(name);
  }
  m_priv->m_stack.push(context);
  if(xmlTextReaderIsEmptyElement(reader)) {
    endElement(reader);
  }
  return true;
}

bool PlistXmlParser::endElement(xmlTextReaderPtr reader)
{
  if(m_priv->m_stack.empty()) {
    // BAD
    fprintf(stderr, "empty stack\n");
    return false;
  }

  XmlContext * top_context = m_priv->m_stack.top();
  const xmlChar *name = xmlTextReaderConstName(reader);
  if(!xmlStrEqual(name, BAD_CAST top_context->element.c_str())) {
    // BAD
    fprintf(stderr, "inconsistent stack, Expected %s, got %s\n", 
	    top_context->element.c_str(), (const char*)name);
    return false;
  }
  m_priv->m_stack.pop();
  
  bool ret = true;
  if(!m_priv->m_stack.empty()) {
    XmlContext *parent_context = m_priv->m_stack.top();
    
    if(DictXmlContext *dict = dynamic_cast<DictXmlContext*>(parent_context)) {
      if(KeyXmlContext* key = dynamic_cast<KeyXmlContext*>(top_context)) {
	dict->last_key = key->key;
      }
      else if(ValXmlContext* val = dynamic_cast<ValXmlContext*>(top_context)) {
	std::string & last_key = dict->last_key;
	if(!last_key.empty()) {
	  dict->dict->insert(std::make_pair(last_key, val->value));
	  last_key.clear();
	}
      }
    }
    else if(ArrayXmlContext *array = dynamic_cast<ArrayXmlContext*>(parent_context)) {
      if(ValXmlContext* val = dynamic_cast<ValXmlContext*>(top_context)) {
	array->array->push_back(val->value);
      }
    }
    else if(parent_context->element == "plist") {
      DictXmlContext *dict = dynamic_cast<DictXmlContext*>(top_context);
      if(dict) {
	m_priv->m_plist = dict->dict;
      }
    }
  }
  delete top_context;
  return ret;
}

PlistDict::Ptr PlistXmlParser::_parse(xmlTextReaderPtr reader)
{
  int ret = 0;
  do {
    ret = xmlTextReaderRead(reader);
    switch(xmlTextReaderNodeType(reader))
    {
    case XML_READER_TYPE_ELEMENT:
      if(!startElement(reader)) {
	ret = -1;
      }
      break;
    case XML_READER_TYPE_TEXT:
      if(!readText(reader)) {
	ret = -1;
      }
      break;
    case XML_READER_TYPE_END_ELEMENT:
      if(!endElement(reader)) {
	ret = -1;
      }
      break;
    default:
      break;
    }
  } while(ret == 1);
  
  return m_priv->m_plist;
}
