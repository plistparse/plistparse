

#include "plist-dict.hpp"
#include "plist-any.hpp"


PlistDict::PlistDict()
{
}


PlistDict::~PlistDict()
{
}


PlistDict::mapped_type PlistDict::get_property(const PlistDict::key_type & k) const
{
	const_iterator iter = find(k);
	if(iter == end()) {
		return PlistAny();
	}
	return iter->second;
}


std::string PlistDict::to_string() const
{
  std::string output = "{\n";

  for(const_iterator iter = begin(); iter != end(); ++iter) {
		const PlistAny & value = iter->second;
    output += iter->first + " = " + value.to_string() + "\n";
	}
  output += "}";
  return output;
}

void PlistDict::dump(FILE * out)
{
  if(out == NULL) {
    out = stdout;
  }
  fprintf(out, "%s\n", to_string().c_str());
}
