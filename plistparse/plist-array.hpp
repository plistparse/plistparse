
#ifndef __PLIST_ARRAY_HPP__
#define __PLIST_ARRAY_HPP__

#include <string>
#include <vector>
#include <tr1/memory>

class PlistAny;

class PlistArray
	: public std::vector<PlistAny>
{
public:
	typedef std::tr1::shared_ptr<PlistArray> Ptr;

  std::string to_string() const;
};


#endif
