

#include <boost/lexical_cast.hpp>
#include "plist-any.hpp"


class ToStringVisitor
  : public boost::static_visitor<std::string>
{
public:
  std::string operator()(int64_t n) const
    {
      return boost::lexical_cast<std::string>(n);
    }
  std::string operator()(bool b) const
    {
      return b ? "true" : "false";
    }
  std::string operator()(double d) const
    {
      return boost::lexical_cast<std::string>(d);
    }
  std::string operator()(const std::string & s) const
    {
      return s;
    }
  std::string operator()(const PlistDict::Ptr & d) const
    {
      return d->to_string();
    }
  std::string operator()(const PlistArray::Ptr & a) const
    {
      return a->to_string();
    }
};


class TypeVisitor 
	: public boost::static_visitor<PLIST_TYPE>
{
public:
    PLIST_TYPE operator()(int64_t) const
    {
        return PLIST_TYPE_INT;
    }

    PLIST_TYPE operator()(bool) const
    {
        return PLIST_TYPE_BOOL;
    }
	
    PLIST_TYPE operator()(double) const
    {
        return PLIST_TYPE_REAL;
    }
    
    PLIST_TYPE operator()(const std::string & ) const
    {
        return PLIST_TYPE_STRING;
    }

    PLIST_TYPE operator()(const PlistDict::Ptr & ) const
    {
        return PLIST_TYPE_DICT;
    }

    PLIST_TYPE operator()(const PlistArray::Ptr & ) const
    {
        return PLIST_TYPE_ARRAY;
    }
};


PlistAny::PlistAny()
{
}

PlistAny::PlistAny(const std::string &s)
	: _inner_type(s)
{
}

PlistAny::PlistAny(int64_t n)
	: _inner_type(n)
{
}

PlistAny::PlistAny(bool b)
	: _inner_type(b)
{
}

PlistAny::PlistAny(double r)
: _inner_type(r)
{
}

PlistAny::PlistAny(const PlistDict::Ptr & pl)
	: _inner_type(pl)
{
}

PlistAny::PlistAny(const PlistArray::Ptr & pa)
: _inner_type(pa)
{
}

PlistAny::PlistAny(const PlistAny & rhs)
	: _inner_type(static_cast<const _inner_type&>(rhs))
{
}

PLIST_TYPE PlistAny::type() const
{
	return boost::apply_visitor( TypeVisitor(), *static_cast<const _inner_type*>(this));
}

std::string PlistAny::to_string() const
{
  return boost::apply_visitor( ToStringVisitor(), *static_cast<const _inner_type*>(this));
}

