

#ifndef __PLIST_ANY_HPP__
#define __PLIST_ANY_HPP__

#include <boost/variant.hpp>

#include "plist-dict.hpp"
#include "plist-array.hpp"

class PlistDict;

enum PLIST_TYPE {
	PLIST_TYPE_NONE = 0,
	PLIST_TYPE_STRING = 1,
	PLIST_TYPE_INT = 2,
	PLIST_TYPE_DICT = 3,
	PLIST_TYPE_ARRAY = 4,
	PLIST_TYPE_BOOL = 5,
	PLIST_TYPE_REAL = 6,
	
	_PLIST_TYPE_LAST
};

class PlistAny
	: private boost::variant<std::string, int64_t, bool, double, PlistDict::Ptr, PlistArray::Ptr>
{
	typedef boost::variant<std::string, int64_t, bool, double, PlistDict::Ptr, PlistArray::Ptr> _inner_type;
	
public:
	PlistAny();
	PlistAny(const std::string &);
	PlistAny(int64_t);
	PlistAny(bool);
	PlistAny(double);
	PlistAny(const PlistDict::Ptr &);
	PlistAny(const PlistArray::Ptr &);
	
	PlistAny(const PlistAny &);
	
	PLIST_TYPE type() const;
  std::string to_string() const;
};


#endif
