


#ifndef __PLIST_PARSER_HPP__
#define __PLIST_PARSER_HPP__

#include <string>

#include "plist-dict.hpp"

/** Abstract Plist parser */
class PlistParser
{
public:

  virtual PlistDict::Ptr parseFile(const std::string & filename) = 0;
  virtual PlistDict::Ptr parseFile(const char * filename) = 0;


};

#endif
