
#include "plist-array.hpp"
#include "plist-any.hpp"

std::string PlistArray::to_string() const
{
  std::string output = "{\n";

  for(const_iterator iter = begin(); iter != end(); ++iter) {
    output += iter->to_string() + "\n";
	}
  output += "}";
  return output;  
}
