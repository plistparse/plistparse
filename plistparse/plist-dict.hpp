


#ifndef __PLIST_DICT_HPP__
#define __PLIST_DICT_HPP__

#include <cstdio>
#include <map>
#include <string>
#include <utility>
#include <tr1/memory>

class PlistAny;

class PlistDict
	: private std::map<std::string,PlistAny>
{
	typedef std::map<std::string,PlistAny>  _inner_type;
	
public:

	typedef std::tr1::shared_ptr<PlistDict> Ptr;
	
	typedef _inner_type::key_type           key_type;
	typedef _inner_type::mapped_type        mapped_type;
	typedef _inner_type::value_type         value_type;
	typedef _inner_type::iterator           iterator;
	typedef _inner_type::const_iterator     const_iterator;
	typedef _inner_type::size_type          size_type;
	
	PlistDict();
	~PlistDict();
	
	const_iterator begin() const
		{ return _inner_type::begin(); }
	const_iterator end() const
		{ return _inner_type::end(); }
	
	mapped_type get_property(const key_type & k) const;
	
	std::pair<iterator, bool> insert(const value_type& __x)
		{ return _inner_type::insert(__x); }
	
	size_type erase(const key_type& __x)
		{ return _inner_type::erase(__x); }
	
	size_type size() const
		{ return _inner_type::size(); }

  std::string to_string() const;
// debug
  void dump(FILE * );	
};

#endif
