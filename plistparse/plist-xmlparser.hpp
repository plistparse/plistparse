

#ifndef __PLIST_XMLPARSER_HPP__
#define __PLIST_XMLPARSER_HPP__

#include <libxml/xmlreader.h>

#include <string>

#include "plist-array.hpp"
#include "plist-parser.hpp"

class PlistXmlParser
  : public PlistParser
{
	
public:
  PlistXmlParser();
  virtual ~PlistXmlParser();
  
  virtual PlistDict::Ptr parseFile(const std::string & filename);
  virtual PlistDict::Ptr parseFile(const char * filename);
	
private:
  //
  bool startElement(xmlTextReaderPtr reader);
  bool endElement(xmlTextReaderPtr reader);
  bool readText(xmlTextReaderPtr reader);

  PlistDict::Ptr _parse(xmlTextReaderPtr reader);

  class Priv;
  Priv * m_priv;
};

#endif
