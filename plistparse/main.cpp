

#include <stdio.h>

#include "plist-xmlparser.hpp"
#include "plist-dict.hpp"
#include "plist-any.hpp"

int main(int argc, const char **argv)
{
  PlistXmlParser parser;
  
  if(argc < 2) {
    fprintf(stderr, "not enough arguments\n");
    return 1;
  }
  
  PlistDict::Ptr plist = parser.parseFile(argv[1]);
  if(!plist) {
    fprintf(stderr, "couldn't parse file %s\n", argv[1]);
    return 1;
  }
  printf("plist size = %lu\n", plist->size());
  
  plist->dump(stdout);

  return 0;
}
